#!/bin/bash
tmp_file=$(mktemp formatlist.XXXX)

function cleanup() {
    rm "$tmp_file"
}

# Remove temp file after running
trap cleanup EXIT

# Get all the files changes between devel and now
base_commit=$(git merge-base HEAD origin/main)
git diff --name-only "$base_commit" | grep "\.swift$" | while read filename; do
    [[ -e "$filename" ]] || continue; # Ignore if file doesn't exist
    echo "$filename" >> "$tmp_file"
done

[[ -s $tmp_file ]] || exit 0; # Only run if file is not empty
swiftformat --filelist "$tmp_file" --swiftversion 5.4 --minversion 0.48