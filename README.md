# IMDBLand

## Requirements

You'll need the following tools to be able to run the project

- [Bundler](https://bundler.io)
- [Brew](https://brew.sh)
- [Cocoapods](https://cocoapods.org)
- [Tuist](https://tuist.io)

However, run the following command to install them

```sh
sh boostrap
```

It will install the required tools and dependencies.

## Project modules

The following image represents the dependency graph within the project

![Dependency graph](graph.png)

## Run the project

The project uses Tuist to generate the project and configure its modules.

Run the following command:

```sh
tuist generate --open
```

You can always run only:
```sh
tuist generate
```

and then open `IMDBLand.xcworkspace`.

### Note

- Please note, that if you change your branch, please re-open the project using `tuist generate`. Project doesn't support `tuist focus [target]`

## To-Do

There are some features that hasn't been completed such as:
- Unit and snapshot testing. However, most of the things were done having this in mind and are easy to mock, since most of the definitions does not depend on concrete implementation but in abstractions.
- A better way to query items from disk storage
- Favorited movies screen