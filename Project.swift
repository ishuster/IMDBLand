import ProjectDescription
import ProjectDescriptionHelpers

/*
                +-------------+
                |             |
                |     App     | Contains IMDBLand App target and IMDBLand unit-test target
                |             |
         +------+-------------+-------+
         |         depends on         |
         |                            |
 +----v-----+                   +-----v-----+
 |          |                   |           |
 |   Kit    |                   |     UI    |   Two independent frameworks to share code and start modularising your app
 |          |                   |           |
 +----------+                   +-----------+

 */

// MARK: - Project

// Creates our project using a helper function defined in ProjectDescriptionHelpers
let project: Project = {
    let project = Project.app(
        name: "IMDBLand",
        dependencies: [.cocoapods(path: ".")],
        platform: .iOS,
        additionalTargets: ["IMDBLandKit", "IMDBLandUI", "IMDBLandNetworking"],
        actions: [.swiftLint, .swiftFormat]
    )
    return project
}()
