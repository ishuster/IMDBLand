//
//  TargetAction+Custom.swift
//  ProjectDescriptionHelpers
//
//  Created by shuster on 12/12/21.
//

import ProjectDescription

public extension TargetAction {
    static func pre(optionalCommand: String, name: String?, invocation: String) -> TargetAction {
        let name = name ?? optionalCommand
        var script = ""

        script += """

        if ! hash \(optionalCommand); then
            echo "\(name) not detected, skipping"
            exit 0;
        fi

        \(invocation)
        """
        return .pre(script: script,
                    name: name,
                    basedOnDependencyAnalysis: true)
    }

    static var swiftFormat: TargetAction {
        let invocation = "$SRCROOT/xcode_swiftformat.sh"
        return Self.pre(optionalCommand: "swiftformat", name: "Swift Format", invocation: invocation)
    }

    static var swiftLint: TargetAction {
        return Self.pre(optionalCommand: "swiftlint", name: "SwiftLint", invocation: "swiftlint")
    }
}
