//
//  NetworkingClient.swift
//  IMDBLand
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation
import IMDBLandKit
import IMDBLandNetworking

protocol NetworkingClient {
    func requestLatestMovies(completion: @escaping MoviesCompletion)
    func requestPopularMovies(completion: @escaping MoviesCompletion)
    func requestUpcomingMovies(completion: @escaping MoviesCompletion)
}

final class DefaultLandNetworkClient: NetworkingClient {
    private let coreClient = NetworkingCore.makeLandNetworkingCoreClient(session: .shared)

    func requestPopularMovies(completion: @escaping MoviesCompletion) {
        let endpoint = PopularMoviesEndpoint()
        coreClient.request(endpoint: endpoint) { (result: Result<MovieResponse, Error>) in
            switch result {
            case .success(let response):
                completion(.success(response.results))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func requestUpcomingMovies(completion: @escaping MoviesCompletion) {
        let endpoint = UpcomingMoviesEndpoint()
        coreClient.request(endpoint: endpoint) { (result: Result<MovieResponse, Error>) in
            switch result {
            case .success(let response):
                completion(.success(response.results))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func requestLatestMovies(completion: @escaping MoviesCompletion) {
        let year = Calendar.current.dateComponents([.year], from: Date()).year ?? 2021
        let endpoint = LatestMoviesEndpoint(year: year)
        coreClient.request(endpoint: endpoint) { (result: Result<MovieResponse, Error>) in
            switch result {
            case .success(let response):
                completion(.success(response.results))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
