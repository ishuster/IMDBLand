//
//  LatestMoviesEndpoint.swift
//  IMDBLand
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation
import IMDBLandNetworking

struct LatestMoviesEndpoint: EndpointConvertible {
    var path: String {
        return "/3/discover/movie"
    }

    var parameters: Parameters? {
        return ["sort_by": "release_date.desc", "year": year]
    }

    var httpMethod: HTTPMethod = .get

    let year: Int

    init(year: Int) {
        self.year = year
    }
}
