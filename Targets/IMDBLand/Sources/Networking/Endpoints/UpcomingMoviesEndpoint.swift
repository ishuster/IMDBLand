//
//  UpcomingMoviesEndpoint.swift
//  IMDBLand
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation
import IMDBLandNetworking

struct UpcomingMoviesEndpoint: EndpointConvertible {
    var path: String {
        return "/3/movie/upcoming"
    }

    var parameters: Parameters?

    var httpMethod: HTTPMethod = .get
}
