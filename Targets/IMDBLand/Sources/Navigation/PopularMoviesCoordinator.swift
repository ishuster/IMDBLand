//
//  PopularMoviesCoordinator.swift
//  IMDBLand
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

final class PopularMoviesCoordinator: Coordinator {
    let navigationController: UINavigationController
    let networkingClient: NetworkingClient
    let movieRepository: MovieRepository
    init(networkingClient: NetworkingClient, movieRepository: MovieRepository) {
        self.networkingClient = networkingClient
        self.movieRepository = movieRepository
        navigationController = UINavigationController()
    }

    func start() {
        let viewController = MoviesViewController(movieRepository: movieRepository, source: .popular)
        viewController.delegate = self
        let title = "Popular"
        viewController.navigationItem.title = title
        viewController.view.backgroundColor = .systemPurple
        navigationController.tabBarItem.title = title
        navigationController.tabBarItem.image = .init(systemName: "film")
        navigationController.pushViewController(viewController, animated: false)
    }
}

extension PopularMoviesCoordinator: MoviesViewControllerDelegate {
    func moviesViewController(_ viewController: MoviesViewController, didSelectMovie detailViewModel: MovieDetailViewModeling) {
        let detailViewController = MovieDetailViewController(viewModel: detailViewModel, image: nil)
        navigationController.pushViewController(detailViewController, animated: true)
    }
}
