//
//  UpcomingMoviesCoordinator.swift
//  IMDBLand
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

final class UpcomingMoviesCoordinator: Coordinator {
    let navigationController: UINavigationController
    let networkingClient: NetworkingClient
    let movieRepository: MovieRepository
    init(networkingClient: NetworkingClient, movieRepository: MovieRepository) {
        self.networkingClient = networkingClient
        self.movieRepository = movieRepository
        navigationController = UINavigationController()
    }

    func start() {
        let viewController = MoviesViewController(movieRepository: movieRepository, source: .upcoming)
        viewController.delegate = self
        let title = "Upcoming"
        viewController.navigationItem.title = title
        viewController.view.backgroundColor = .white
        navigationController.tabBarItem.title = title
        navigationController.tabBarItem.image = .init(systemName: "calendar.badge.clock")
        navigationController.viewControllers = [viewController]
    }
}

extension UpcomingMoviesCoordinator: MoviesViewControllerDelegate {
    func moviesViewController(_ viewController: MoviesViewController, didSelectMovie detailViewModel: MovieDetailViewModeling) {
        let detailViewController = MovieDetailViewController(viewModel: detailViewModel, image: nil)
        navigationController.pushViewController(detailViewController, animated: true)
    }
}
