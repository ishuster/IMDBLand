//
//  LatestMoviesCoordinator.swift
//  IMDBLand
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

final class LatestMoviesCoordinator: Coordinator {
    let navigationController: UINavigationController
    let networkingClient: NetworkingClient
    let movieRepository: MovieRepository
    init(networkingClient: NetworkingClient, movieRepository: MovieRepository) {
        self.networkingClient = networkingClient
        self.movieRepository = movieRepository
        navigationController = UINavigationController()
    }

    func start() {
        let viewController = MoviesViewController(movieRepository: movieRepository, source: .latest)
        viewController.delegate = self
        let title = "Latest"
        viewController.navigationItem.title = title
        navigationController.tabBarItem.title = title
        navigationController.tabBarItem.image = .init(systemName: "newspaper")
        navigationController.pushViewController(viewController, animated: false)
    }
}

extension LatestMoviesCoordinator: MoviesViewControllerDelegate {
    func moviesViewController(_ viewController: MoviesViewController, didSelectMovie detailViewModel: MovieDetailViewModeling) {
        let detailViewController = MovieDetailViewController(viewModel: detailViewModel, image: nil)
        navigationController.pushViewController(detailViewController, animated: true)
    }
}
