//
//  StoreProcessing.swift
//  IMDBLand
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation
import IMDBLandKit

protocol StoreProcessing {
    func save(_ movies: [Movie], key: String)
    func restore(fromKey: String) -> [Movie]?
}

final class StoreProcessor: StoreProcessing {
    init() {}
    func save(_ movies: [Movie], key: String) {
        Storage.store(movies, to: .documents, as: key)
    }

    func restore(fromKey: String) -> [Movie]? {
        return Storage.retrieve(fromKey, from: .documents, as: [Movie].self)
    }
}
