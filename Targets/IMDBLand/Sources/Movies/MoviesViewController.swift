//
//  MoviesViewController.swift
//  IMDBLand
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//
import IMDBLandKit
import IMDBLandUI
import UIKit

protocol MoviesViewControllerDelegate: AnyObject {
    func moviesViewController(_ viewController: MoviesViewController, didSelectMovie detailViewModel: MovieDetailViewModeling)
}

final class MoviesViewController: UIViewController {
    private let contentView = MoviesView()
    let movieRepository: MovieRepository

    enum Source {
        case latest
        case popular
        case upcoming
    }

    enum Section: Int {
        case main
    }

    weak var delegate: MoviesViewControllerDelegate?

    typealias Model = MovieCellViewModel
    typealias DataSource = UICollectionViewDiffableDataSource<Section, Model>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Model>
    private var dataSource: DataSource!

    let source: Source

    init(movieRepository: MovieRepository, source: Source) {
        self.movieRepository = movieRepository
        self.source = source
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = contentView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        updateDataSource(movies: [], animatingDifferences: false)
        contentView.beginRefreshing()
        fetchMovies(repositorySource: .local)
        contentView.didStartRefreshing = { [weak self] in
            guard let self = self else { return }
            self.fetchMovies(repositorySource: .remote)
        }
    }
}

// MARK: - Fetching data -

extension MoviesViewController {
    func fetchMovies(repositorySource: RepositorySource) {
        let animated = view.window != nil
        let updateDataSourceHandler: (Result<[Movie], Error>) -> Void = { [weak self] result in
            switch result {
            case .success(let movies):
                self?.updateDataSource(movies: movies, animatingDifferences: animated)

            case .failure(let error):
                self?.presentErrorAlert(error: error, onQueue: .main) {
                    self?.fetchMovies(repositorySource: repositorySource)
                }
            }
            DispatchQueue.main.async {
                self?.contentView.endRefreshing()
            }
        }

        print("Fetching source: \(source)")
        switch source {
        case .latest:
            movieRepository.fetchLatestMovies(source: repositorySource, completion: updateDataSourceHandler)

        case .popular:
            movieRepository.fetchPopularMovies(source: repositorySource, completion: updateDataSourceHandler)

        case .upcoming:
            movieRepository.fetchUpcomingMovies(source: repositorySource, completion: updateDataSourceHandler)
        }
    }

    func presentErrorAlert(error: Error, onQueue queue: DispatchQueue, retryHandler: @escaping () -> Void) {
        queue.async {
            let title = "Something went wrong"
            let message = error.localizedDescription
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default) { _ in }
            let retryAction = UIAlertAction(title: "Retry", style: .default) { _ in
                retryHandler()
            }

            alertController.addAction(cancelAction)
            alertController.addAction(retryAction)
            alertController.preferredAction = retryAction
            self.present(alertController, animated: true)
        }
    }
}

// MARK: - Data source -

private extension MoviesViewController {
    func setupDataSource() {
        contentView.collectionViewDelegate = self
        contentView.register(cellType: MovieCollectionViewCell.self)
        dataSource = DataSource(collectionView: contentView.collectionView, cellProvider: { collectionView, indexPath, itemIdentifier in
            let cell = collectionView.dequeueCell(for: indexPath) as MovieCollectionViewCell
            cell.configure(viewModel: itemIdentifier)
            return cell
        })
    }

    func updateDataSource(movies: [Movie], animatingDifferences: Bool = true) {
        precondition(dataSource != nil, "Please setup first dataSource. Using 'setupDataSource()'")
        let viewModels = movies.map(MovieCellViewModel.init)
        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(viewModels, toSection: .main)
        DispatchQueue.main.async {
            self.dataSource.apply(snapshot, animatingDifferences: animatingDifferences) {
                print("✅ applied snapshot for movies.count: \(viewModels.count)\n")
            }
        }
    }
}

// MARK: - UICollectionViewDelegate -

extension MoviesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let itemIdentifier = dataSource.itemIdentifier(for: indexPath),
              let movie = movieRepository.movie(byIdentifier: itemIdentifier.id) else {
            return
        }

        let detailViewModel = MovieDetailViewModel(movie: movie, posterlUrl: itemIdentifier.posterPath)
        delegate?.moviesViewController(self, didSelectMovie: detailViewModel)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout -

extension MoviesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var padding: CGFloat = 40.0
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            padding = (layout.sectionInset.left + layout.sectionInset.right) * 1.5
        }
        let width = (collectionView.frame.width - padding) / 2
        let size = CGSize(width: width, height: 300)
        return size
    }
}
