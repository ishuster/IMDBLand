//
//  MovieCellViewModeling.swift
//  IMDBLand
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import IMDBLandKit
import IMDBLandNetworking
import UIKit

protocol MovieCellViewModeling {
    var id: Int { get }
    var title: String { get }
    var voteAverage: String { get }
    var posterPath: URL? { get }
    var placeholderImage: UIImage? { get }
}

struct MovieCellViewModel: MovieCellViewModeling {
    let movie: Movie
    init(movie: Movie) {
        self.movie = movie
    }

    var id: Int {
        return movie.id
    }

    var title: String {
        return movie.title
    }

    var voteAverage: String {
        return "\(movie.voteAverage)"
    }

    var posterPath: URL? {
        guard let posterPath = movie.posterPath, !posterPath.isEmpty else {
            return nil
        }

        guard var url = URL(string: Environment.imageBaseUrlString) else {
            return nil
        }

        url.appendPathComponent(posterPath)

        return url
    }

    var placeholderImage: UIImage? {
        return UIImage(systemName: "film")
    }
}

extension MovieCellViewModel: Hashable {}

extension MovieCellViewModel: Equatable {}
