//
//  MovieDetailViewModeling.swift
//  IMDBLand
//
//  Created by shuster on 15/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation
import IMDBLandKit

protocol MovieDetailViewModeling {
    var name: String { get }
    var rating: String { get }
    var synopsis: String { get }
    var posterUrl: URL? { get }
}

struct MovieDetailViewModel: MovieDetailViewModeling {
    var name: String {
        return movie.title
    }

    var rating: String {
        return "Rating: \(movie.voteAverage)"
    }

    var synopsis: String {
        return movie.overview
    }

    var posterUrl: URL?

    let movie: Movie

    init(movie: Movie, posterlUrl: URL?) {
        self.movie = movie
        posterUrl = posterlUrl
    }
}
