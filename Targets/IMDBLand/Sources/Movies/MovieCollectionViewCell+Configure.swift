//
//  MovieCollectionViewCell+Configure.swift
//  IMDBLand
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import IMDBLandUI
import Kingfisher
import UIKit

extension MovieCollectionViewCell {
    func configure(viewModel: MovieCellViewModeling) {
        titleLabel.text = viewModel.title
        imageView.kf.indicatorType = .activity
        let url = viewModel.posterPath
        guard let url = url else {
            imageView.contentMode = .center
            imageView.image = viewModel.placeholderImage
            return
        }

        imageView.contentMode = .scaleAspectFill
        imageView.kf.setImage(with: url) { result in
            switch result {
            case .success(let retrieveImageResult):
                print("imaged downloaded successfully")

            case .failure(let kingfisherError):
                print(kingfisherError)
            }
        }
    }
}
