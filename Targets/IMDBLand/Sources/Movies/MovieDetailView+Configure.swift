//
//  MovieDetailView+Configure.swift
//  IMDBLand
//
//  Created by shuster on 15/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import IMDBLandUI
import UIKit

import Kingfisher
extension MovieDetailView {
    func configure(viewModel: MovieDetailViewModeling) {
        configure(name: viewModel.name, rating: viewModel.rating, synopsis: viewModel.synopsis, image: nil)
        guard let url = viewModel.posterUrl else { return }
        posterImageView.kf.setImage(with: url) { result in
            switch result {
            case .success(let retrieveImageResult):
                print("imaged downloaded successfully")
                self.posterImageView.isHidden = false
                self.layoutIfNeeded()

            case .failure(let kingfisherError):
                print(kingfisherError)
            }
        }
    }
}
