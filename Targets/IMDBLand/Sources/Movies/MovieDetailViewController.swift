//
//  MovieDetailViewController.swift
//  IMDBLand
//
//  Created by shuster on 15/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import IMDBLandKit
import IMDBLandUI
import UIKit

final class MovieDetailViewController: UIViewController {
    private lazy var detailView = MovieDetailView()
    private let image: UIImage?
    private let viewModel: MovieDetailViewModeling
    init(viewModel: MovieDetailViewModeling, image: UIImage? = nil) {
        self.viewModel = viewModel
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = detailView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        detailView.configure(viewModel: viewModel)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        detailView.didUpdateLayout()
    }
}
