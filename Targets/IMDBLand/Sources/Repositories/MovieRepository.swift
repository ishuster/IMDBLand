//
//  MovieRepository.swift
//  IMDBLand
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation
import IMDBLandKit

typealias MoviesCompletion = (Result<[Movie], Error>) -> Void

protocol MovieRepository {
    func fetchLatestMovies(source: RepositorySource, completion: @escaping MoviesCompletion)
    func fetchPopularMovies(source: RepositorySource, completion: @escaping MoviesCompletion)
    func fetchUpcomingMovies(source: RepositorySource, completion: @escaping MoviesCompletion)
    func movie(byIdentifier identifier: Int) -> Movie?
}

enum RepositorySource {
    case local
    case remote
}
