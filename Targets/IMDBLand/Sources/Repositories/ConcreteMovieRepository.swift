//
//  ConcreteMovieRepository.swift
//  IMDBLand
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation
import IMDBLandKit

final class ConcreteMovieRepository: MovieRepository {
    let networkingClient: NetworkingClient

    private let queue: DispatchQueue = {
        let queue = DispatchQueue(label: "dev.alekscbarragan.movierepository")
        return queue
    }()

    private var moviesByIdentifier = [Int: Movie]()

    private let storeProcessor = StoreProcessor()

    init(networkingClient: NetworkingClient) {
        self.networkingClient = networkingClient
    }

    func fetchLatestMovies(source: RepositorySource = .remote, completion: @escaping (Result<[Movie], Error>) -> Void) {
        let key = #function
        if let cachedMovies = restoreIfAvailable(forKey: key, repositorySource: source) {
            queue.async {
                cachedMovies.forEach { movie in
                    self.moviesByIdentifier[movie.id] = movie
                }
                print("🤡 returning latest movies from source: local")
                completion(.success(cachedMovies))
            }
            return
        }

        networkingClient.requestLatestMovies { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let movies):
                self.queue.async {
                    movies.forEach { movie in
                        self.moviesByIdentifier[movie.id] = movie
                    }
                    print("🤡 returning latest movies from source: remote")
                    completion(.success(movies))
                    self.storeProcessor.save(movies, key: key)
                }
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
    }

    func fetchPopularMovies(source: RepositorySource = .remote, completion: @escaping MoviesCompletion) {
        let key = #function
        if let cachedMovies = restoreIfAvailable(forKey: key, repositorySource: source) {
            print("🤡 returning popular movies from source: local")
            processCachedMovies(cachedMovies, completion: completion)
            return
        }

        networkingClient.requestPopularMovies { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let movies):
                self.queue.async {
                    movies.forEach { movie in
                        // key a reference by movie.id
                        self.moviesByIdentifier[movie.id] = movie
                    }
                    print("🤡 returning popular movies from source: remote")
                    completion(.success(movies))
                    self.storeProcessor.save(movies, key: key)
                }
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
    }

    func fetchUpcomingMovies(source: RepositorySource = .remote, completion: @escaping MoviesCompletion) {
        let key = #function
        if let cachedMovies = restoreIfAvailable(forKey: key, repositorySource: source) {
            print("🤡 returning upcoming movies from source: local")
            processCachedMovies(cachedMovies, completion: completion)
            return
        }
        // fallback to remote fetch
        networkingClient.requestUpcomingMovies { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let movies):
                self.queue.async {
                    movies.forEach { movie in
                        self.moviesByIdentifier[movie.id] = movie
                    }
                    print("🤡 returning upcoming movies from source: remote")
                    completion(.success(movies))
                    self.storeProcessor.save(movies, key: key)
                }
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
    }

    func movie(byIdentifier identifier: Int) -> Movie? {
        return moviesByIdentifier[identifier]
    }

    func processCachedMovies(_ movies: [Movie], completion: @escaping MoviesCompletion) {
        queue.async {
            movies.forEach { movie in
                self.moviesByIdentifier[movie.id] = movie
            }
            completion(.success(movies))
        }
    }

    func restoreIfAvailable(forKey key: String, repositorySource: RepositorySource) -> [Movie]? {
        guard repositorySource == .local, let cachedMovies = storeProcessor.restore(fromKey: key), !cachedMovies.isEmpty else {
            return nil
        }

        return cachedMovies
    }
}
