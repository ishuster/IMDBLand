import IMDBLandKit
import IMDBLandUI
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    private lazy var networkingClient: NetworkingClient = {
        return DefaultLandNetworkClient()
    }()

    private lazy var movieRepository: MovieRepository = {
        return ConcreteMovieRepository(networkingClient: networkingClient)
    }()

    private lazy var latestMoviesCoordinator: LatestMoviesCoordinator = {
        let coordinator = LatestMoviesCoordinator(networkingClient: networkingClient, movieRepository: movieRepository)
        return coordinator
    }()

    private lazy var popularMoviesCoordinator: PopularMoviesCoordinator = {
        let coordinator = PopularMoviesCoordinator(networkingClient: networkingClient, movieRepository: movieRepository)
        return coordinator
    }()

    private lazy var upcomingMoviesCoordinator: UpcomingMoviesCoordinator = {
        let coordinator = UpcomingMoviesCoordinator(networkingClient: networkingClient, movieRepository: movieRepository)
        return coordinator
    }()

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)

        // Appearance
        let appearance = UITabBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .systemBackground

        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.backgroundColor = .systemBackground

        latestMoviesCoordinator.start()
        popularMoviesCoordinator.start()
        upcomingMoviesCoordinator.start()

        let navControllers: [UINavigationController] = [
            latestMoviesCoordinator.navigationController,
            popularMoviesCoordinator.navigationController,
            upcomingMoviesCoordinator.navigationController
        ]

        navControllers.forEach { navController in
            navController.navigationBar.tintColor = .systemOrange
            navController.navigationBar.compactAppearance = navBarAppearance
            navController.navigationBar.standardAppearance = navBarAppearance
            navController.navigationBar.scrollEdgeAppearance = navBarAppearance
        }

        let tabBarController = UITabBarController()
        tabBarController.tabBar.standardAppearance = appearance
        tabBarController.tabBar.scrollEdgeAppearance = tabBarController.tabBar.standardAppearance
        tabBarController.viewControllers = navControllers
        tabBarController.tabBar.tintColor = .systemOrange

        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()

        return true
    }
}
