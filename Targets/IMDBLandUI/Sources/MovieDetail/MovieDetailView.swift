//
//  MovieDetailView.swift
//  IMDBLandUI
//
//  Created by shuster on 15/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

public final class MovieDetailView: UIView {
    public private(set) lazy var posterImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    let nameLabel: Label = {
        let label = Label(insets: .label, text: "Movie name")
        label.font = .preferredFont(forTextStyle: .largeTitle)
        return label
    }()

    let ratingLabel: Label = {
        let label = Label(insets: .label, text: "7.5")
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()

    let synopsisLabel: Label = {
        let label = Label(insets: .label, text: "Synopsis")
        label.font = .preferredFont(forTextStyle: .body)
        label.numberOfLines = 0
        return label
    }()

    let scrollView: ArrangedScrollView = {
        let scrollView = ArrangedScrollView()
        return scrollView
    }()

    public func configure(name: String, rating: String, synopsis: String, image: UIImage?) {
        nameLabel.text = name
        ratingLabel.text = rating
        synopsisLabel.text = synopsis
        if let image = image {
            posterImageView.image = image
            posterImageView.isHidden = false
        } else {
            posterImageView.isHidden = true
        }
    }

    public func didUpdateLayout() {
        scrollView.didUpdateLayout()
    }

    // MARK: - Life Cycle

    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        backgroundColor = .systemBackground
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupConstraints()
    }

    // MARK: - Private Methods

    private func setupConstraints() {
        addSubview(scrollView)
        scrollView.edgesToSuperview(usingSafeLayout: true)
        let subviews = [posterImageView, nameLabel, ratingLabel, synopsisLabel]
        scrollView.addArranged(subviews: subviews)
        posterImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.6).isActive = true
    }
}
