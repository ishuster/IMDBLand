//
//  Reusable.swift
//  IMDBLandUI
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation

public protocol Reusable {
    static var reuseIdentifier: String { get }
}

public extension Reusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
