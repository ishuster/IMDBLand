//
//  UICollectionReusableView.swift
//  IMDBLandUI
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

extension UICollectionReusableView: Reusable {}

public extension UICollectionView {
    func register(cellType type: UICollectionViewCell.Type) {
        register(type, forCellWithReuseIdentifier: type.reuseIdentifier)
    }

    func dequeueCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("You need to register cell of type `\(T.reuseIdentifier)`")
        }

        return cell
    }

    func registerSupplementaryView(suplementaryViewType type: UICollectionReusableView.Type, kind: String) {
        register(type, forSupplementaryViewOfKind: kind, withReuseIdentifier: type.reuseIdentifier)
    }

    func dequeueSupplementaryView<T: UICollectionReusableView>(ofKind elementKind: String, for indexPath: IndexPath) -> T {
        guard let supplementaryView = dequeueReusableSupplementaryView(
            ofKind: elementKind,
            withReuseIdentifier: T.reuseIdentifier,
            for: indexPath
        ) as? T else {
            fatalError("You need to register supplementaryView of type `\(T.reuseIdentifier)` for kind `\(elementKind)`")
        }
        return supplementaryView
    }
}
