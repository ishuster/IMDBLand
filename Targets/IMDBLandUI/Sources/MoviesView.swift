//
//  MoviesView.swift
//  IMDBLandUI
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

public final class MoviesView: UIView {
    // MARK: - Public Properties

    public weak var collectionViewDelegate: UICollectionViewDelegate? {
        didSet {
            collectionView.delegate = collectionViewDelegate
        }
    }

    public weak var collectionViewDataSource: UICollectionViewDataSource? {
        didSet {
            collectionView.dataSource = collectionViewDataSource
        }
    }

    public var didStartRefreshing: (() -> Void)?

    public private(set) lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .systemBackground
        collectionView.keyboardDismissMode = .interactive
        return collectionView
    }()

    // MARK: - Life Cycle

    public convenience init() {
        self.init(frame: .zero)
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        backgroundColor = .systemBackground
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(startRefreshing), for: .valueChanged)
        refreshControl.tintColor = .systemOrange
        collectionView.refreshControl = refreshControl
        collectionView.bounces = true
        collectionView.alwaysBounceVertical = true
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupConstraints()
    }

    // MARK: - Private Methods

    private func setupConstraints() {
        addSubview(collectionView)
        collectionView.edgesToSuperview()
    }

    @objc private func startRefreshing() {
        beginRefreshing()
        didStartRefreshing?()
    }

    // MARK: - Public Methods

    public func beginRefreshing() {
        collectionView.refreshControl?.beginRefreshing()
    }

    public func endRefreshing() {
        collectionView.refreshControl?.endRefreshing()
    }

    public func indexPath(for cell: UICollectionViewCell) -> IndexPath? {
        return collectionView.indexPath(for: cell)
    }

    public func register(cellType type: UICollectionViewCell.Type) {
        collectionView.register(cellType: type)
    }

    public func reloadData() {
        collectionView.reloadData()
    }
}
