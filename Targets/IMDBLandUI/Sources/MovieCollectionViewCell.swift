//
//  MovieCollectionViewCell.swift
//  IMDBLandUI
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

public protocol MovieCollectionViewCellDelegate: AnyObject {
    func movieCollectionViewCell(_ cell: MovieCollectionViewCell, didSelectFavorite button: UIButton)
}

public final class MovieCollectionViewCell: BaseCollectionViewCell {
    weak var delegate: MovieCollectionViewCellDelegate?

    public let titleLabel: UILabel = {
        let label: UILabel = .create(text: "Movie")
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()

    public let favoriteButton: UIButton = {
        let button = UIButton()
        let selectedImage = UIImage(systemName: "suit.heart.fill")
        let image = UIImage(systemName: "suit.heart")
        button.setImage(selectedImage, for: .selected)
        button.setImage(image, for: .normal)
        button.tintColor = .systemOrange
        return button
    }()

    public let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .systemBackground
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.tintColor = .systemOrange
        return imageView
    }()

    let infoContainerView: UIView = {
        let view = UIView()
        return view
    }()

    let containerView: UIView = {
        let view = UIView()
        return view
    }()

    override func setup() {
        super.setup()

        contentView.addSubview(containerView)
        containerView.edgesToSuperview(insets: .all(8))

        containerView.addSubview(infoContainerView)
        infoContainerView.leftToSuperView()
        infoContainerView.bottomToSuperview()
        infoContainerView.rightToSuperView()
        infoContainerView.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)

        containerView.addSubview(imageView)
        imageView.topToSuperview()
        imageView.leftToSuperView()
        imageView.rightToSuperView()
        imageView.bottomToTop(of: infoContainerView)
        imageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)

        // Heart button
        containerView.addSubview(favoriteButton)
        favoriteButton.topToSuperview(offset: 10)
        favoriteButton.rightToSuperView(offset: -10)
        favoriteButton.addTarget(self, action: #selector(favoriteButtonTouchUpInside(_:)), for: .touchUpInside)

        infoContainerView.addSubview(titleLabel)
        titleLabel.leftToSuperView()
        titleLabel.topToSuperview()
        titleLabel.bottomToSuperview()
        titleLabel.rightToSuperView()
    }

    @objc private func favoriteButtonTouchUpInside(_ sender: UIButton) {
        UIButton.animateSelection(sender: sender)
        delegate?.movieCollectionViewCell(self, didSelectFavorite: sender)
    }
}
