//
//  UILabel+Create.swift
//  IMDBLandUI
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

extension UILabel {
    static func create(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.defaultConfiguration()
        return label
    }

    func defaultConfiguration() {
        translatesAutoresizingMaskIntoConstraints = false
        textColor = .label
        font = UIFont.systemFont(ofSize: 15)
        numberOfLines = 5
        adjustsFontSizeToFitWidth = true
    }
}

public final class Label: UILabel {
    public let insets: UIEdgeInsets
    public init(insets: UIEdgeInsets = .zero, text: String = "", heightConstant: CGFloat = 0) {
        self.insets = insets
        super.init(frame: .zero)
        self.text = text
        defaultConfiguration()
        if heightConstant > 0 {
            heightAnchor.constraint(equalToConstant: heightConstant).isActive = true
        }
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: insets))
    }
}
