//
//  ArrangedScrollView.swift
//  IMDBLandUI
//
//  Created by shuster on 15/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit

public final class ArrangedScrollView: UIScrollView {
    let stackView: UIStackView = {
        let stack = UIStackView(frame: .zero)
        stack.axis = .vertical
        stack.spacing = 0
        return stack
    }()

    public var insets: UIEdgeInsets = .zero {
        didSet {
            stackView.layoutMargins = insets
        }
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(stackView)
        stackView.edgesToSuperview()
        stackView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        alwaysBounceVertical = true
        stackView.isLayoutMarginsRelativeArrangement = true
        isScrollEnabled = true
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Add all the given UIView instances as arranged subviews
    ///
    /// - Parameter subviews: All the UIView instances to be added as subviews
    public func addArranged(subviews: UIView...) {
        subviews.forEach(stackView.addArrangedSubview)
    }

    public func addArranged(subviews: [UIView]) {
        subviews.forEach(stackView.addArrangedSubview)
    }

    public func didUpdateLayout() {
        contentSize = stackView.frame.size
    }
}
