//
//  UIView+AutoLayout.swift
//  IMDBLandUI
//
//  Created by shuster on 14/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import UIKit
extension UIView {
    func addSubview(_ view: UIView, autolayout: Bool) {
        translatesAutoresizingMaskIntoConstraints = !autolayout
        addSubview(view)
    }

    func edgesToSuperview(usingSafeLayout: Bool = false, insets: UIEdgeInsets = .zero) {
        guard let superview = superview else {
            assertionFailure("A `superview` is needed to set up constraints.")
            return
        }

        prepareForAutolayout()

        if usingSafeLayout {
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor, constant: insets.top).isActive = true
            trailingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.trailingAnchor, constant: -insets.right).isActive = true
            bottomAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.bottomAnchor, constant: -insets.bottom).isActive = true
            leadingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leadingAnchor, constant: insets.left).isActive = true
        } else {
            topAnchor.constraint(equalTo: superview.topAnchor, constant: insets.top).isActive = true
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -insets.right).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -insets.bottom).isActive = true
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: insets.left).isActive = true
        }
    }

    // MARK: - Right

    /// This method is used to configure the relationship between user interface object to the right that must be satisfied by
    /// the constraint-based layout system. The assigned object to the right of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - anchor: Constraint to set the trailing edge of the given component.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func right(to view: UIView,
               anchor: NSLayoutXAxisAnchor? = nil,
               offset: CGFloat = 0,
               activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: rightAnchor, equalTo: anchor ?? view.rightAnchor, offset: offset, activate: activate)
    }

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the right of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func rightToLeft(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: rightAnchor, equalTo: view.leftAnchor, offset: -offset, activate: activate)
    }

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the right of the super view.
    ///
    /// - Parameters:
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func rightToSuperView(offset: CGFloat = 0, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }

        return makeConstraint(from: rightAnchor, equalTo: superview.rightAnchor, offset: offset, activate: activate)
    }

    // MARK: - Left

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the left of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - anchor: Constraint to set the leading edge of the given component.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func left(to view: UIView,
              anchor: NSLayoutXAxisAnchor? = nil,
              offset: CGFloat = 0,
              activate: Bool = true) -> NSLayoutConstraint {
        let viewAnchor = anchor ?? view.leftAnchor
        return makeConstraint(from: leftAnchor, equalTo: viewAnchor, offset: offset, activate: activate)
    }

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the left to right of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func leftToRight(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: leftAnchor, equalTo: view.rightAnchor, offset: offset, activate: activate)
    }

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the left to right of the super view.
    ///
    /// - Parameters:
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func leftToSuperView(offset: CGFloat = 0, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        return makeConstraint(from: leftAnchor, equalTo: superview.leftAnchor, offset: offset, activate: activate)
    }

    // MARK: - Top

    /// This method is used to configure the relationship between user interface object to the right that must be satisfied by
    /// the constraint-based layout system. The assigned object to the top of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - anchor: Constraint to set the top edge of the given component.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - safeArea: Use safe areas as an aid to laying out your content.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func top(to view: UIView,
             anchor: NSLayoutYAxisAnchor? = nil,
             offset: CGFloat = 0,
             safeArea: Bool = false,
             activate: Bool = true) -> NSLayoutConstraint {
        let neighborAnchor: NSLayoutYAxisAnchor
        if safeArea {
            neighborAnchor = anchor ?? view.safeAreaLayoutGuide.topAnchor
        } else {
            neighborAnchor = anchor ?? view.topAnchor
        }

        return makeConstraint(from: topAnchor, equalTo: neighborAnchor, offset: offset, activate: activate)
    }

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the top to bottom of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func topToBottom(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: topAnchor, equalTo: view.bottomAnchor, offset: offset, activate: activate)
    }

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the top of the super view.
    ///
    /// - Parameters:
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - safeArea: Use safe areas as an aid to laying out your content.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func topToSuperview(offset: CGFloat = 0, safeArea: Bool = false, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }

        let neighborAnchor: NSLayoutYAxisAnchor = safeArea ? superview.safeAreaLayoutGuide.topAnchor : superview.topAnchor

        return makeConstraint(from: topAnchor, equalTo: neighborAnchor, offset: offset, activate: activate)
    }

    // MARK: - Bottom

    /// This method is used to configure the relationship between user interface object to the right that must be satisfied by
    /// the constraint-based layout system. The assigned object to the bottom of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - anchor: Constraint to set the bottom edge of the given component.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func bottom(to view: UIView,
                anchor: NSLayoutYAxisAnchor? = nil,
                offset: CGFloat = 0,
                activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: bottomAnchor, equalTo: anchor ?? view.bottomAnchor, offset: offset, activate: activate)
    }

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the bottom to top of the given view.
    ///
    /// - Parameters:
    ///   - view: UIView Object that will be used as base to set your object to the right.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func bottomToTop(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: bottomAnchor, equalTo: view.topAnchor, offset: offset, activate: activate)
    }

    /// This method is used to configure the relationship between two user interface objects that must be satisfied by
    /// the constraint-based layout system. The assigned object to the bottom of the super view.
    ///
    /// - Parameters:
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    @discardableResult
    func bottomToSuperview(offset: CGFloat = 0, activate: Bool = true) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        return makeConstraint(from: bottomAnchor, equalTo: superview.bottomAnchor, offset: offset, activate: activate)
    }

    // MARK: - Private

    /// This method created a constraint that defines the horizontal position of two item's.
    ///
    /// - Parameters:
    ///   - anchor: Constraint that defines a constant offset.
    ///   - neighborAnchor: Constraint that defines one item's attribute as equal to another item's attribute.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    private func makeConstraint(from anchor: NSLayoutXAxisAnchor,
                                equalTo neighborAnchor: NSLayoutXAxisAnchor,
                                offset: CGFloat,
                                activate: Bool) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalTo: neighborAnchor, constant: offset)
        constraint.isActive = activate
        translatesAutoresizingMaskIntoConstraints = false
        return constraint
    }

    /// This method created a constraint that defines the vertical position of two item's.
    ///
    /// - Parameters:
    ///   - anchor: Constraint that defines a constant offset.
    ///   - neighborAnchor: Constraint that defines one item's attribute as equal to another item's attribute.
    ///   - offset: The constant added to the attribute participating in the constraint.
    ///   - activate: The active state of the constraint.
    /// - Returns: NSLayoutConstraint
    private func makeConstraint(from anchor: NSLayoutYAxisAnchor,
                                equalTo neighborAnchor: NSLayoutYAxisAnchor,
                                offset: CGFloat,
                                activate: Bool) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalTo: neighborAnchor, constant: offset)
        constraint.isActive = activate
        translatesAutoresizingMaskIntoConstraints = false
        return constraint
    }

    private func prepareForAutolayout() {
        translatesAutoresizingMaskIntoConstraints = false
    }
}
