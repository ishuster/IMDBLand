//
//  LandNetworkingCore.swift
//  IMDBLandNetworking
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation

public protocol LandNetworkingCore {
    func request<Model: Codable>(endpoint: EndpointConvertible, completion: @escaping (Result<Model, Error>) -> Void)
}

public enum NetworkingCore {
    public static func makeLandNetworkingCoreClient(session: URLSession) -> LandNetworkingCore {
        return LandNetworkingCoreClient(session: session)
    }
}

final class LandNetworkingCoreClient: LandNetworkingCore {
    let session: URLSession

    init(session: URLSession) {
        self.session = session
    }

    func request<Model: Codable>(endpoint: EndpointConvertible, completion: @escaping (Result<Model, Error>) -> Void) {
        do {
            let request = try endpoint.asURLRequest()
            session.dataTask(with: request) { data, _, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }

                guard let data = data else {
                    completion(.failure(LandNetworkingError.nilData))
                    return
                }

                if Environment.current == .debug {
                    let string = String(data: data, encoding: .utf8)
//                    print("👀 response\n\(string as Any)")
                }

                do {
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Model.self, from: data)
                    completion(.success(model))
                } catch {
                    completion(.failure(error))
                }
            }.resume()
        } catch {
            completion(.failure(error))
        }
    }
}
