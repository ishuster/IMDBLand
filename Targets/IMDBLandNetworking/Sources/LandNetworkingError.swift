//
//  LandNetworkingError.swift
//  IMDBLandNetworking
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation

public enum LandNetworkingError: Swift.Error {
    case nilData
}
