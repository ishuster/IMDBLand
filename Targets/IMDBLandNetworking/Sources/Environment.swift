//
//  Environment.swift
//  IMDBLandNetworking
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation

public enum Environment {
    case debug
    case release

    public static var current: Environment {
        #if DEBUG
        return .debug
        #else
        return .release
        #endif
    }

    public static var baseURLString: String {
        switch current {
        case .debug:
            return "https://api.themoviedb.org/"

        case .release:
            // TODO: Replace production URL.
            return "https://api.themoviedb.org/"
        }
    }

    public static var imageBaseUrlString: String {
        switch current {
        case .debug:
            return "https://image.tmdb.org/t/p/original"

        case .release:
            // TODO: Replace production URL.
            return "https://image.tmdb.org/t/p/original"
        }
    }

    public static var apiKey: String {
        return "adeefc70c416e66c04fdc31abf034d9d"
    }
}
