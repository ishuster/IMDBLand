//
//  EndpointConvertible.swift
//  IMDBLandNetworking
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation

public typealias Parameters = [String: Any]
public protocol EndpointConvertible {
    var apiKey: String { get }
    var baseUrlString: String { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var httpMethod: HTTPMethod { get }
    func asURLRequest() throws -> URLRequest
}

public extension EndpointConvertible {
    var apiKey: String {
        return Environment.apiKey
    }

    var baseUrlString: String {
        return Environment.baseURLString
    }

    func asURLRequest() throws -> URLRequest {
        guard var components = URLComponents(string: baseUrlString) else {
            throw EndpointError.malformedUrl(string: baseUrlString)
        }
        components.path = path
        var queryItems: [URLQueryItem] = []
        queryItems.append(.init(name: "api_key", value: apiKey))
        if let parameters = parameters, !parameters.isEmpty {
            queryItems += parameters.map { key, value in URLQueryItem(name: key, value: "\(value)") }
        }

        components.queryItems = queryItems
        let url = components.url

        guard let anUrl = url else {
            throw EndpointError.malformedUrl(string: url?.absoluteString ?? baseUrlString)
        }
        return URLRequest(url: anUrl)
    }
}
