//
//  Movie.swift
//  IMDBLandKit
//
//  Created by shuster on 12/12/21.
//  Copyright © 2021 alekscbarragan.dev. All rights reserved.
//

import Foundation

public struct Movie: Codable, Identifiable, Hashable {
    public let popularity: Double
    public let voteCount: Int
    public let posterPath: String?
    public let originalTitle: String
    public let title: String
    public let id: Int
    public let genreIds: [Int]
    public let voteAverage: Double
    public let overview: String
    public let releaseDate: String

    public var isFavorited: Bool? = false

    enum CodingKeys: String, CodingKey {
        case popularity
        case voteCount = "vote_count"
        case posterPath = "poster_path"
        case originalTitle = "original_title"
        case title
        case id
        case genreIds = "genre_ids"
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
        // custom for encoding
        case isFavorited
    }
}
